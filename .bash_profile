################################################################################
# # from caveats of `brew install nvm`
#   export NVM_DIR="$HOME/.nvm"
#     [ -s "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh" ] && . "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh"  # This loads nvm
#       [ -s "/home/linuxbrew/.linuxbrew/opt/nvm/etc/bash_completion" ] && . "/home/linuxbrew/.linuxbrew/opt/nvm/etc/bash_completion"  # This loads nvm bash_completion
# 
# # from caveats of `brew install node@8`
# export PATH="/home/linuxbrew/.linuxbrew/opt/node@8/bin:$PATH"
eval $(/home/linuxbrew/.linuxbrew/bin/brew shellenv)

# add newline to prompt
PS1="$PS1\n"
alias muse='find -L ~/Music/ -type f -print0 | grep -v jpg -z | sort -Rz | xargs -0 totem & '

alias rfile='nextfile="."; while [ -d "$nextfile" ]; do nextfile="$nextfile"/"`ls -a """$nextfile""" | sort -R | head -n 1`"; echo $nextfile; done'

alias ff=firefox

# export NVM_DIR="$HOME/.nvm"
#[ -s "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh" ] && . "/home/linuxbrew/.linuxbrew/opt/nvm/nvm.sh"  # This loads nvm
#[ -s "/home/linuxbrew/.linuxbrew/opt/nvm/etc/bash_completion" ] && . "/home/linuxbrew/.linuxbrew/opt/nvm/etc/bash_completion"  # This loads nvm bash_completion

# Load nodenv automatically by appending

# eval "$(nodenv init -)"

if [ -e /home/qwelk/.nix-profile/etc/profile.d/nix.sh ]; then . /home/qwelk/.nix-profile/etc/profile.d/nix.sh; fi # added by Nix installer

source ~/.bashrc
