" Mostly from: https://stevelosh.com/blog/2010/09/coming-home-to-vim/#s11-important-vimrc-lines
set undofile
nnoremap / /\v
vnoremap / /\v
set ignorecase
set smartcase
set gdefault
set incsearch
set showmatch
set hlsearch
set colorcolumn=85
set listchars=tab:▸\ ,eol:¬
